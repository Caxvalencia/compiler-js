export const Operators = {
    EPSILON: '#',
    ZERO_OR_MANY: '*',
    ONE_OR_MANY: '+',
    OR: '|',
    PARENTHESIS_OPEN: '(',
    PARENTHESIS_CLOSE: ')',
};
